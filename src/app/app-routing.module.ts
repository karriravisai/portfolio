import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BlogComponent } from './blog/blog.component';
import { LandingComponent } from './landing/landing.component';
import { ProfileComponent } from './profile/profile.component';

const routes: Routes = [
  {path: '', component: LandingComponent},
  { path: '*', component: LandingComponent },
  { path: 'portfolio/landing', component: LandingComponent },
  { path: 'portfolio/profile', component: ProfileComponent },
  { path: 'portfolio/blog', component: BlogComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
