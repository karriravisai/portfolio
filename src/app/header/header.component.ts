import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
})
export class HeaderComponent {
  public rotate: any;
  constructor(
    private router: Router
  ) { }

  twist() {
    this.rotate = !this.rotate;
  }

  profile() {
    this.router.navigate(['portfolio/profile'])
      .then(nav => {
        console.log(nav); // true if navigation is successful
      }, err => {
        console.log(err) // when there's an error
      });
  }

  profile2() {
    this.router.navigate(['portfolio/blog'])
      .then(nav => {
        console.log(nav); // true if navigation is successful
      }, err => {
        console.log(err) // when there's an error
      });
  }

  home() {
    this.router.navigate(['portfolio/landing'])
  }

}
