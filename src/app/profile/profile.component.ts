import { Component } from '@angular/core';

@Component({
  selector: 'profile',
  templateUrl: './profile.component.html',
})
export class ProfileComponent {
  constructor() {}

  prev = () => {
    console.log('prev');
  };

  next = () => {
    console.log('next');
  };
}
